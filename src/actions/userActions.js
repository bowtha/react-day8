export const FETCH_USER  = 'FETCH_USER';
export const SEARCH_NAME = 'SEARCH_NAME';
export const SET_USER = 'SET_USER';

export const fetchUser = () => {
    return {
        type: FETCH_USER
    } 
}
export const searchName = (newName) => {
    return {
        type: SEARCH_NAME ,
        payLoad: newName
    } 
}
export const setUser = (data) => {
    return {
        type: SET_USER,
        payLoad: data
    } 
}