import { SEARCH_NAME,SET_USER } from '../actions/userActions'


const initialUser={
    users: [],
    searchText: ""
}


export const userReducer = (state = initialUser, action) => {
    switch (action.type) {
        case SEARCH_NAME:
            return {...state, searchText : action.payLoad}

        case SET_USER:
        
            return {...state, users : action.payLoad}

        default:
            return state
    }
}

