import React from 'react'
import { useSelector } from 'react-redux';
import { List, Avatar, Skeleton, Icon } from 'antd';
import '../css/userPage.css';
import 'antd/dist/antd.css';



const UserList = () => {

    const usersInfo = useSelector(state => state.users);
    const dataUser = usersInfo.users
    const text = usersInfo.searchText
    console.log(text)
    return (
        <div>
            <List
                className="demo-loadmore-list"
                itemLayout="horizontal"
                dataSource={
                    text == '' ?
                        dataUser
                        :
                        dataUser.filter(data => (data.name.match(text)))
                }

                renderItem={item => (
                    <List.Item
                        actions={[<a href={"/users/" + item.id + "/todo"}><Icon type="unordered-list" /></a>
                        ]}
                    >
                        <Skeleton avatar title={false} loading={item.loading} active>
                            <List.Item.Meta
                                avatar={
                                    <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />
                                }
                                title={<p>{item.name}</p>}
                                description={item.email}
                            />
                        </Skeleton>
                    </List.Item>
                )}
            />
        </div>
    )
}

export default UserList


