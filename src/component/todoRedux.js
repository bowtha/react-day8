import React, { useEffect } from 'react';
import TodoList from './todoList';
import { useDispatch } from 'react-redux';
import { fetchTodo } from '../reducers/todoReducer';
import { PageHeader , Button} from 'antd';
import SearchTodo from './SearchTodo'



const TodoRedux = (props) => {

    const userID = props.match.params.user_id;
    
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchTodo(userID))
    }, [])

    return (
        <div>
             <PageHeader
                style={{
                    border: '1px solid rgb(235, 237, 240)',
                }}
                onBack={() => { window.location = "/users" }}
                title="Back"
            />

            <h1>Todo Redux</h1>
            <SearchTodo/>
            <div>
                <TodoList />
            </div>
            <Button type="danger" onClick={() => { window.location = "/processLogout" }} >Logout</Button> 
        </div>
    );
}

export default TodoRedux;

