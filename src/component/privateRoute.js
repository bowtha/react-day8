import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ path, component: Component, ...props }) => {
     return(
        <Route path={path} render={(props) => {

            const isLogin = localStorage.getItem("isLogin")

            if (isLogin == "true") {
                return <Component {...props} />
            }
            else {
                return <Redirect to="/login" />
            }
        }} {...props}/>
    )
}

export default PrivateRoute;
