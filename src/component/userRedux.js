import React, { useEffect } from 'react';
import { Typography,Button } from 'antd';
import 'antd/dist/antd.css';
import { useDispatch } from 'react-redux';
import '../css/userPage.css';
import { setUser } from '../actions/userActions'
import UserList from './userList';
import SearchUser from './searchUser';

// const { Search } = Input;
const { Title } = Typography;

const UserPage = () => {

    const dispatch = useDispatch();

    useEffect(() => {

        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {

                dispatch(setUser(data))
            })

    }, [])

    return (
        <div>
            <Title>All USERS</Title>
            <SearchUser />
            <UserList />
            <Button type="danger" onClick={() => { window.location = "/processLogout" }} >Logout</Button> 
        </div>
    );
}

export default UserPage;